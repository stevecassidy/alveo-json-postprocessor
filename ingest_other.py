'''
Created on 7 Sep 2016

@author: Michael
'''

import os,re,json,shutil,glob,sys
from rdflib import Graph,Namespace,Literal,URIRef
from __builtin__ import str
import argparse
import time
from datetime import datetime

# SRC_DIR = '/mnt/volume/austalk/austalk-published/metadata'
SRC_DIR = '/mnt/volume/austalk/austalk-published/metadata'

RDF_FORMAT = 'nt'
FILES_METADATA_SUFFIX = "-metadata"
DOWNSAMPLED_FILE_SUFFIX = "-ds"
METADATA_FILE_SUFFIX = "-metadata"
DEFAULT_CHUNK_SIZE = 5000
DEFAULT_CHUNK_SIZE = 100

ALVEO = Namespace(u"http://alveo.edu.au/vocabulary/")

ALVEO_ITEM_URL = u'https://app.alveo.edu.au/catalog/austalk/%(collection)s/%(item)s'
ALVEO_DOCUMENT_URL = u'https://app.alveo.edu.au/catalog/%(collection)s/%(item)s/document/%(document)s'
ALVEO_SPEAKER_URL = u'https://app.alveo.edu.au/speakers/%(collection)s/%(collection)s/person/%(person)s'

def item_uri(collection, item):
    return ALVEO_ITEM_URL % {'collection': collection, 'item': item}

def document_uri(collection, item, document):
    document = document.replace(' ', '_')
    return ALVEO_DOCUMENT_URL % {'collection': collection, 'item': item, 'document': document}

def speaker_uri(collection, item, ident):
    return ALVEO_SPEAKER_URL % {'collection': collection, 'item': item, 'person': ident}


# NAMESPACES
RDF = Namespace(u"http://www.w3.org/1999/02/22-rdf-syntax-ns#")
AUSNC = Namespace("http://ns.ausnc.org.au/schemas/ausnc_md_model/")


#Create context stuffs, I'm assuming contexts are constant
ITEM_CONTEXT = {
            "dcterms": "http://purl.org/dc/terms/",
            "austalk": "http://ns.austalk.edu.au/",
            "olac": "http://www.language-archives.org/OLAC/1.1/",
            "ausnc": "http://ns.ausnc.org.au/schemas/ausnc_md_model/",
            "foaf": "http://xmlns.com/foaf/0.1/",
            "alveo": "http://alveo.edu.au/vocabulary/",
            "austalkid":"http://id.austalk.edu.au/",
            "rdf":"http://www.w3.org/1999/02/22-rdf-syntax-ns#",
            "dada":"http://purl.org/dada/schema/0.2#",
            "dbpedia":"http://dbpedia.org/resource/",
            "bio":"http://purl.org/vocab/bio/0.1/",
            "cooeeann": "http://ns.ausnc.org.au/schemas/annotation/cooee/",
            "cooee": "http://ns.ausnc.org.au/schemas/cooee/",
            "ace": "http://ns.ausnc.org.au/schemas/ace/",
            "bibo": "http://purl.org/ontology/bibo/",
            "artann": "http://ns.ausnc.org.au/schemas/annotation/art/",
            "schema": "http://ns.ausnc.org.au/schemas/",

            "dcterms:isPartOf":  {"@type": "@id"},
            "ausnc:document": {"@type": "@id"},
            "alveo:display_document": {"@type": "@id"},
            "alveo:indexable_document": {"@type": "@id"},
            "austalk:speech_style": {"@type": "@id"},
            "austalk:information_giver": {"@type": "@id"},
            "austalk:component": {"@type": "@id"},
            "austalk:prototype": {"@type": "@id"},
            "austalk:map": {"@type": "@id"},
            "olac:speaker": {"@type": "@id"},
            "austalk:information_follower": {"@type": "@id"},
            "austalk:birthPlace": {"@type": "@id"},
            "austalk:father_birthPlace": {"@type": "@id"},
            "austalk:mother_birthPlace": {"@type": "@id"},
            "austalk:residential_history": {"@type": "@id"},
            "austalk:first_language": {"@type": "@id"},
            "austalk:father_first_language": {"@type": "@id"},
            "austalk:mother_first_language": {"@type": "@id"},
            "austalk:recording_site": {"@type": "@id"},
            "austalk:language_usage": {"@type": "@id"},
            "olac:recorder": {"@type": "@id"},
            "dada:annotates": {"@type": "@id"},
            "dada:targets": {"@type": "@id"},
            "dada:partof": {"@type": "@id"},
            "dada:type": {"@type": "@id"},

           }

def get_files(srcdir, item_pattern=''):
    ''' This function generates a sequence of files that
    the Austalk ingest should actually process
    Note this is a generator (using yield)'''

    print "get_files: srcdir[%s], item_pattern[%s]" %(srcdir, item_pattern)

    res = []
    src_depth = len(srcdir.split(os.path.sep))
    for root, dirnames, filenames in os.walk(srcdir):
        for filename in filenames:
            if re.match(item_pattern, filename):
                yield os.path.join(root, filename)


def get_item_files(srcdir):
    ''' This function generates a sequence of files that
    the Austalk ingest should actually process
    Note this is a generator (using yield)'''
    print "get_item_files: srcdir[%s]" %(srcdir)

    item_pattern = ".*"+FILES_METADATA_SUFFIX+"\.nt"
    for i in get_files(srcdir, item_pattern):
        yield i

def clear_output(outdir):
    ''' Clears the output file '''
    if os.path.exists(outdir):
        shutil.rmtree(outdir)
    os.mkdir(outdir)

def files_to_graphs(files):
    '''Generate RDF graphs for a list of RDF files'''
    graphs = []
    count = 0
    for filename in files:
        print "FILE", filename
        g = Graph()
        g.parse(filename, format=RDF_FORMAT)

        (basename, ext) = os.path.splitext(filename)
        graphs.append((g,basename.split(os.sep)[-1]))

        count += 1

    return graphs


def item_file_to_graph(filename):
    ''' Generate RDF graphs for a list of item -files.nt files
    this needs to look for other associated files and group them all
    together. '''

    print "item_file_to_graph: filename[%s]" %(filename)

    item_metadata = filename.replace(FILES_METADATA_SUFFIX, "")

    g = Graph()

    # concatenate all metadata files with pattern XXXX-foo.nt (eg. XXXX-metadata.nt, XXXX-ann.nt)
    (basename, ext) = os.path.splitext(item_metadata)
    itemfiles = glob.glob(basename + "-*.nt")

    for ifile in itemfiles:
        g.parse(ifile, format=RDF_FORMAT)

    #graphs.append((g,basename.split(os.sep)[-1]))
    return g, basename.split(os.sep)[-1]



def fix_context(predicate, value, CONTEXT=ITEM_CONTEXT):

    return (prefix(predicate, CONTEXT), prefix(value, CONTEXT))


# memoize this function
prefix_m = dict()
unknown_uri = set()

def prefix(uri_or_literal, context):

    global prefix_m

    if uri_or_literal in prefix_m:
        return prefix_m[uri_or_literal]

    result = uri_or_literal

    if type(uri_or_literal) == Literal:
        # don't cache literals or data URIs
        return uri_or_literal
    else:
        for key, val in context.items():
            if type(val) == str and str(uri_or_literal).startswith(val):
                result = uri_or_literal.replace(val, key+":",1)
                break

    if result == uri_or_literal and uri_or_literal.startswith('http') and not 'app.alveo' in uri_or_literal:
        unknown_uri.add(uri_or_literal)

    prefix_m[uri_or_literal] = result

    return result


def graph_speakers_to_json(graphs, indent=False):
    ''' Process an rdf graph into a JSON-ND file '''
    result = {'items':[]}
    for graph,itemName in graphs:
        #put all metadata here, remember ausnc:document is a list
        #and will count towards the ausnc:document sibling of metadata

        item = {"@context":[SPEAKER_CONTEXT],'@graph':[],'alveo:metadata':{},'ausnc:document':[]}

        for subject in graph.subjects():
            meta = graph_subject_to_json(graph, subject)
            item['@graph'].append(meta)

        result['items'].append(item)

    return json.dumps(result, indent=indent)

def graph_subject_to_json(graph, subject):

    meta = {}
    data = graph.triples((URIRef(subject), None, None))
    for s, p,o in data:
        p,o = fix_context(p, o)
        if type(o) == Literal and o.datatype == URIRef(u'http://www.w3.org/2001/XMLSchema#integer'):
            meta[p] = int(o)
        else:
            meta[p] = o

    # set the subject
    meta['@id'] = subject

    if 'rdf:type' in meta:
        meta['@type'] = meta['rdf:type']
        meta.pop('rdf:type')
        if meta['@type'] == 'foaf:Person':
            meta['@id'] = subject

    elif 'protocol' in subject:
        #  Item in the protocol doesn't have a type
        meta['@type'] = 'austalk:Item'

    # special case for empty comments
    if 'austalk:comment' in meta and meta['austalk:comment'] == '':
        meta.pop('austalk:comment')

    return meta


def graph_item_to_json(collection, graph, itemName, indent=None):
    ''' Process an rdf graph into a JSON-ND file '''

    #put all metadata here, remember ausnc:document is a list
    #and will count towards the ausnc:document sibling of metadata
    metadata = {}
    item = {"@context":[ITEM_CONTEXT],'@graph':[],'alveo:metadata':{},'ausnc:document':[]}

    metadata['ausnc:document'] = []

    documents = []

    # should be just one item in this graph
    itemURI = list(graph.subjects(RDF.type, AUSNC.AusNCObject))[0]
    subjects_processed = [itemURI]

    metadata['@id'] = itemURI

    res = graph.triples((itemURI, None, None))

    # we go looking for this
    fulltextdoc = None

    for subject,predicate,value in res:

        predicate, value = fix_context(predicate,value)

        #ausnc:document is special and needs to be a list
        if predicate=='ausnc:document':
            metadata['ausnc:document'].append(value)
            documents.append(value)
        elif predicate=='alveo:indexable_document':
            fulltextdoc = value
            metadata[predicate] = value
        elif predicate=='rdf:type':
            metadata['@type'] = value
        else:
            metadata[predicate] = value

    # ensure that this is set
    metadata['dcterms:isPartOf'] = collection

    item['alveo:metadata'] = metadata

    #Add documents
    for doc in documents:
        docmeta = {}
        #res = graph.predicate_objects(URIRef(doc))

        res = graph.triples((URIRef(doc), None, None))

        subjects_processed.append(URIRef(doc))

        #needs to point locally
        docmeta['@id'] = doc

        for s, predicate,value in res:

            predicate,value = fix_context(predicate,value)

            if predicate=='rdf:type':
                docmeta['@type'] = value
            elif type(value) == Literal and value.datatype == URIRef(u'http://www.w3.org/2001/XMLSchema#integer'):
                docmeta[predicate] = int(value)
            elif predicate=='dcterms:source':
                # dcterms:source should be the full path to the file
                docmeta['dcterms:source'] = os.path.abspath(os.path.join(DATA_DIR, value))
            else:
                docmeta[predicate] = value

        item['ausnc:document'].append(docmeta)

        if doc == fulltextdoc:
            with open(docmeta['dcterms:source']) as fd:
                fulltext = fd.read()
            item['alveo:metadata']['alveo:full_text'] = fulltext

    # mop up any extra subjects (eg. recordedsession)
    for subject in graph.subjects():
        if not subject in subjects_processed:
            subjects_processed.append(subject)
            meta = graph_subject_to_json(graph, subject)
            item['@graph'].append(meta)

    return json.dumps(item, indent=indent)

def process_speakers(src_dir, output_dir, indent=None):

    start_time = time.time()
    tally = 0
    count = 0
    for files in get_speaker_files(src_dir):
        #get all the .nt files and process them into graphs
        graphs = files_to_graphs(files)
        tally += len(graphs)
        #write out the graph into a json format
        json_string = graph_speakers_to_json(graphs, indent=indent)

        #Open up the destination JSON file and write json output
        with open(os.path.join(output_dir, "speaker-%d.json" % count),"w") as out_json:
            out_json.write(json_string)
        count += 1

    print "Processed %d speaker files in %s" % (tally, time.time()-start_time)


def output_items(items, output_dir, count):

    with open(os.path.join(output_dir, "items-%d.json" % count),"w") as out_json:
        out_json.write('{"items":\n[\n')
        out_json.write(',\n'.join(items))
        out_json.write('\n]\n}')


def process_items(collection, src_dir, output_dir, indent=None):

    print "process_items: collection[%s], src_dir[%s], output_dir[%s], indent[%s]\n" %(collection, src_dir, output_dir, indent)

    start_time = time.time()
    print "start_time[%s]" %(str(datetime.now()))

    tally = 0
    count = 0
    items = []
    for fname in get_item_files(src_dir):
        #get all the .nt files and process them into a graph
        print "%s processing..." %(fname)

        graph, itemName = item_file_to_graph(fname)
        tally += 1
        #write out the graph into a json format
        json_string = graph_item_to_json(collection, graph, itemName, indent=indent)

        items.append(json_string)
        if len(items) >= DEFAULT_CHUNK_SIZE:
            output_items(items, output_dir, count)
            count += 1
            items = []

    if items != []:
        output_items(items, output_dir, count)
    print "Processed %d items in %s" % (tally, time.time()-start_time)

def parser():
    parser = argparse.ArgumentParser(description="Process Austalk RDF Files for Alveo ingest")
    parser.add_argument('--indent', required=False, action="store_const", const=2, default=None, help="indent JSON")
    parser.add_argument('--clear', required=False, action="store_const", const=True, default=False, help="remove previous JSON output")
    parser.add_argument('--output_dir', required=True, action="store", type=str, help="Output Directory")
    parser.add_argument('collection', action="store", type=str, help="collection name")
    parser.add_argument('input', action="store", type=str, help="Input Directory")
    return parser.parse_args()


if __name__ == '__main__':

    args = parser()

    # set global for data location
    DATA_DIR = args.input

    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    if args.clear:
        print "Clearing old JSON"
        clear_output(args.output_dir)

    process_items(args.collection, args.input, args.output_dir, indent=args.indent)

    print "Unknown URIs:"
    for uri in unknown_uri:
        print uri

    print "Finished, Exiting Program"
